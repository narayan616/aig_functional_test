package selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.IMethodInstance;
import org.testng.annotations.Test;
import selenium.base.Base;

import java.util.concurrent.TimeUnit;

public class HomePage extends Base{
    @Test
    public void testHeader1() throws InterruptedException {
        Thread.sleep(5000);
        String head1= driver.findElement(By.xpath("/html/body/div[1]/div[2]/header/div[1]/nav[1]/div/div/div/div[1]/ul/li[1]/a")).getText();
        Assert.assertEquals(head1,"INDIVIDUALS & FAMILIES");
        String head2=driver.findElement(By.cssSelector(".utilityleftlink1")).getText();
        Assert.assertEquals(head2,"BUSINESS & ENTERPRISE");
        String carrees=driver.findElement(By.xpath("(//*[text()=\"Careers\"])[1]")).getText();
        Assert.assertEquals(carrees,"CAREERS");
        String investor=driver.findElement(By.xpath("(//*[text()=\"Investors\"])[1]")).getText();
        Assert.assertEquals(investor, "INVESTORS");
        String aboutUs= driver.findElement(By.xpath("(//*[text()=\"About Us\"])[1]")).getText();
        Assert.assertEquals(aboutUs,"ABOUT US" );
    }
    @Test
    public void testHeader2(){
        String insurance= driver.findElement(By.xpath("(//*[text()=\"Insurance\"])[1]")).getText();
        Assert.assertEquals(insurance,"INSURANCE" );
        String investments= driver.findElement(By.xpath("(//*[text()=\"Investments\"])[1]")).getText();
        Assert.assertEquals(investments,"INVESTMENTS" );
        String logIn= driver.findElement(By.xpath("(//*[text()=\"Log in\"])[2]")).getText();
        Assert.assertEquals(logIn,"LOG IN" );
    }
    @Test
    public void testPage(){
        String message= driver.findElement(By.xpath("//*[@id='module-heroimage']/div/div/p")).getText();
        Assert.assertEquals(message, "Helping you prepare for times of uncertainty is at the heart of what we do.");
    }
    @Test
    public void footerAssert(){
        String aboutAIG= driver.findElement(By.xpath("//*[@id='header1']")).getText();
        Assert.assertEquals(aboutAIG,"About AIG");
        String legal= driver.findElement(By.xpath("//*[@id='header2']")).getText();
        Assert.assertEquals(legal,"Legal");
        String support= driver.findElement(By.xpath("//*[@id='header3']")).getText();
        Assert.assertEquals(support,"Support");

    }


}
